package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Province struct {
	ProvinceCode int    `json:"provinceCode"`
	ProvinceName string `json:"provinceName"`
}

func main() {
	file, err := ioutil.ReadFile("provinces.json")
	if err != nil {
		fmt.Printf("read file error: %s", err)
		return
	}

	provinces := []Province{}

	err = json.Unmarshal([]byte(file), &provinces)
	if err != nil {
		fmt.Printf("Unmarshal error: %s", err)
		return
	}

	PrintLog(provinces)
}

// PrintLog for print Json log to console...
func PrintLog(n interface{}) {
	b, _ := json.MarshalIndent(n, "", "\t")
	os.Stdout.Write(b)
}
